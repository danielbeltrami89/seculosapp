package br.com.micromais.seculosapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

/**
 * Main activity demonstrating how to pass extra parameters to an activity that
 * reads barcodes.
 */
public class PreLeitorActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";

    private CompoundButton useFlash;

    private EditText digCod = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_leitor);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        useFlash = (CompoundButton) findViewById(R.id.use_flash);
        findViewById(R.id.read_barcode).setOnClickListener(this);

        digCod = (EditText)findViewById(R.id.digCod);
        findViewById(R.id.btnok).setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.read_barcode) {
            // launch barcode activity.
            Intent intent = new Intent(this, LeitorCodActivity.class);
            //intent.putExtra(LeitorCodActivity.AutoFocus,true); //);  focus sempre true
            //intent.putExtra("usarflash", useFlash.isChecked());
            startActivityForResult(intent, RC_BARCODE_CAPTURE);

            SharedPreferences sharedPreferences = getSharedPreferences(PrincipalActivity.TAG_SALVAR_GERAL, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(PrincipalActivity.TAG_FLASH, useFlash.isChecked());
            editor.apply(); //salva nas preferencias o ultimo estado do flash

        }
        else if(v.getId() == R.id.btnok){
            String codBarras = digCod.getText().toString();
            Intent intent = new Intent(this, WVBarrasActivity.class);
            intent.putExtra(WVBarrasActivity.parmUrl, "http://" + PrincipalActivity.IpServer + PrincipalActivity.URL_COD_BARRAS  + "?login="+ PrincipalActivity.Usuario+"&senha="+ PrincipalActivity.Senha+"&cdProduto="+ codBarras); // Param URL para o WebView

            startActivity(intent);
            finish();

        }
    }


    //botao voltar
    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), PrincipalActivity.class);
        startActivity(intent);

    }

}
