package br.com.micromais.seculosapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ConfigActivity extends AppCompatActivity implements View.OnClickListener  {

    EditText ipUrl;
    EditText usuario;
    EditText senha;
    private String ipValor = null;
    private String loginValor;
    private String senhaValor;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        ipUrl = (EditText) findViewById(R.id.ip_config);
        usuario = (EditText)findViewById(R.id.t_usuario);
        senha = (EditText)findViewById(R.id.t_senha);

        SharedPreferences sp1 =getSharedPreferences(PrincipalActivity.TAG_SALVAR_GERAL, MODE_PRIVATE);
        ipValor = sp1.getString(PrincipalActivity.TAG_IP,"");
        ipUrl.setText(ipValor);
        loginValor = sp1.getString(PrincipalActivity.TAG_LOGIN,"");
        usuario.setText(loginValor);
        senhaValor = sp1.getString(PrincipalActivity.TAG_SENHA,"");
        senha.setText(senhaValor);

        Button continuar = (Button)findViewById(R.id.confirm);
        continuar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        ipValor = ipUrl.getText().toString();


        if (validaIp(ipValor)) {

            loginValor = usuario.getText().toString();
            senhaValor = senha.getText().toString();
            SharedPreferences sharedPreferences = getSharedPreferences(PrincipalActivity.TAG_SALVAR_GERAL, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(PrincipalActivity.TAG_IP, ipValor);
            editor.putString(PrincipalActivity.TAG_LOGIN, loginValor);
            editor.putString(PrincipalActivity.TAG_SENHA, senhaValor);

            editor.apply();

            Toast.makeText(this, "Configuração salva com sucesso", Toast.LENGTH_LONG).show();

            onBackPressed();
        }
        else {
            Toast.makeText(this, "Digite um ip válido", Toast.LENGTH_LONG).show();
        }
    }

    //botao voltar
    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), PrincipalActivity.class);
        startActivity(intent);
        finish();
    }

    public boolean validaIp (String ip){
        boolean result = true;
        try {

            //separar por partes ip
            ip = ip.replace(".", "#");//troca ponto por # para poder separar, pq ponto é expressao regular
            String[] parts = ip.split("#");

            //valida tamnaho
            if (parts.length != 4)
                result = false;

            //valida cada parte
            for (String p : parts) {
                if (Integer.valueOf(p) < 0 || Integer.valueOf(p) > 255)
                    result = false;
            }

            return result;
        }
        catch (Exception e){
            Toast.makeText(this, "Digite um ip válido", Toast.LENGTH_LONG).show();
            return false;
        }

    }
}
