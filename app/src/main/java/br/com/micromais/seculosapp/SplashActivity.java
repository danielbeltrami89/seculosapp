package br.com.micromais.seculosapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.*;
import android.widget.ImageView;
//import br.com.micromais.seculosapp.PreLeitorActivity;
//import br.com.micromais.seculosapp.barcodereader.R;

/**
 * Created by Daniel on 30/12/2015.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        final ImageView iv = (ImageView) findViewById(R.id.imageView);
        final Animation an = AnimationUtils.loadAnimation((getBaseContext()),R.anim.fade_in);

        iv.startAnimation(an);
        an.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                finish();
                Intent i = new Intent(getBaseContext(),PrincipalActivity.class);
                startActivity(i);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
