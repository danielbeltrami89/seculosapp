package br.com.micromais.seculosapp;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WVBarrasActivity extends AppCompatActivity {

    public static final String parmUrl = "ParametroUrlConsulta";


    private String urlConsulta;
    protected WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wvbarras);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }




        ConnectivityManager cManager = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo= cManager.getActiveNetworkInfo();
        if(nInfo !=null && nInfo.isConnected()){
            urlConsulta = getIntent().getStringExtra(parmUrl) ;

            wv = (WebView) findViewById(R.id.webView2);

            WebSettings ws = wv.getSettings();
            ws.setJavaScriptEnabled(true);
            ws.setSupportZoom(false);

            WebView myWebView = (WebView) findViewById(R.id.webView2); //criar cliente webview, forcar abrir url in webview
            myWebView.setWebViewClient(new WebViewClient());

            if (savedInstanceState == null)
            {
                // Load a page
                wv.loadUrl(urlConsulta);
            }
        } else{
            Toast.makeText(this,"Sem internet \n Verifique sua conexão wifi!",Toast.LENGTH_LONG).show();
            onBackPressed();

        }





     }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // Salva o estado da WebView
        wv.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        // Restaura o estado da WebView
        wv.restoreState(savedInstanceState);
    }



    //botao voltar
    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), PreLeitorActivity.class);
        startActivity(intent);

    }
}
