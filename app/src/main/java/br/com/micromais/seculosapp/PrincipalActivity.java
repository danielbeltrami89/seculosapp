package br.com.micromais.seculosapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PrincipalActivity extends AppCompatActivity implements View.OnClickListener {

    private int backButtonCount = 0;

    public static final String TAG_SALVAR_GERAL = "Config";   // tag que salva informaçoes de configuraçao
    public static final String TAG_IP = "IpValor";            // tag que salva ip
    public static final String TAG_LOGIN = "loginvalor";      // tag que salva lodin ususario
    public static final String TAG_SENHA = "senhavalor";      // tag que salva senha usuario

    public static final String TAG_FLASH = "usarflash";      // tag que salva senha usuario

    public static final String URL_COD_BARRAS = "/seculos/app/consultarestoque_app.php";                            // url pra consulta de cod barras
    public static final String URL_CONS_ESTOQUE = "/seculos/app/consultarestoque_app2.php";                       // url pra consulta de estoque geral


    public static String IpServer =null;  //ip servidor web
    public static String Usuario;
    public static String Senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //botao para chamar telas de consulta
        Button btnConsulta = (Button) findViewById(R.id.btnConsulta);
        btnConsulta.setOnClickListener(this);
        Button btnEstoque = (Button) findViewById(R.id.btnEstoque);
        btnEstoque.setOnClickListener(this);

       //busca ip configuracao
        SharedPreferences sp1 =getSharedPreferences(TAG_SALVAR_GERAL, MODE_PRIVATE);
        IpServer = sp1.getString(TAG_IP,null);
        Usuario = sp1.getString(TAG_LOGIN,"");
        Senha = sp1.getString(TAG_SENHA,"");

    }

    //acao dos botoes na tela
    @Override
    public void  onClick(View v){
        switch (v.getId()){
            case R.id.btnConsulta:
                if (IpServer == null  ){
                    Toast.makeText(this, "Digite um IP válido em configurações", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent();
                    intent.setClass(getBaseContext(), PreLeitorActivity.class);
                    startActivity(intent);

                }
                break;

            case R.id.btnEstoque:
                if (IpServer == null ){
                    Toast.makeText(this, "Digite um IP válido em configurações", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent inte = new Intent(this, WebviewActivity.class);
                    inte.putExtra(WebviewActivity.parmUrl, "http://" + IpServer + URL_CONS_ESTOQUE+"?login="+Usuario+"&senha="+Senha); // Param URL para o WebView
                    startActivity(inte);
                    //localhost/seculos/app/consultarestoque_app2.php?login=suporte&senha=mmais

                }
                break;

            default:
                break;
        }
    }

    //menu da tela inicial
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();

        if (id == R.id.config){
            Intent intent1 = new Intent();
            intent1.setClass(getBaseContext(),ConfigActivity.class);
            startActivity(intent1);

            return true;
        }
        if (id == R.id.sobre){
            Intent intent = new Intent(this, WebviewActivity.class);
            intent.putExtra(WebviewActivity.parmUrl, "http://micromais.com.br/contato/"); // Param URL para o WebView
            startActivity(intent);


            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //sair do app
    @Override
    public void onBackPressed()
    {
        if(backButtonCount >= 1)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        else
        {
            Toast.makeText(this, "Pressione novamente para sair", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

}
