package br.com.micromais.seculosapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

public class LeitorCodActivity extends AppCompatActivity implements LeitorCod.BarcodeReaderListener {
    private static final String TAG = LeitorCodActivity.class.getSimpleName();

    private LeitorCod leitorCod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leitor);

        // getting barcode instance
        leitorCod = (LeitorCod) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);

        /***
         * Providing beep sound. The sound file has to be placed in
         * `assets` folder
         */
        // leitorCod.setBeepSoundFile("shutter.mp3");

        /**
         * Pausing / resuming barcode reader. This will be useful when you want to
         * do some foreground user interaction while leaving the barcode
         * reader in background
         * */
        // leitorCod.pauseScanning();
        // leitorCod.resumeScanning();
    }

    @Override
    public void onScanned(final Barcode barcode) {
        Log.e(TAG, "onScanned: " + barcode.displayValue);
        leitorCod.playBeep();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String IpServer;  //ip servidor web

                //busca ip configuracao
                SharedPreferences sp1 =getSharedPreferences(PrincipalActivity.TAG_SALVAR_GERAL, MODE_PRIVATE);
                IpServer = sp1.getString(PrincipalActivity.TAG_IP,"");

                Toast.makeText(getApplicationContext(), "Codigo de Barras: " + barcode.displayValue, Toast.LENGTH_SHORT).show();

                //chama a webview e envia cod como parametro
                String codBarras = barcode.displayValue;
                Intent intent = new Intent(getApplicationContext(), WebviewActivity.class);
                intent.putExtra(WebviewActivity.parmUrl, "http://" + IpServer + PrincipalActivity.URL_COD_BARRAS +"?login="+PrincipalActivity.Usuario+"&senha="+PrincipalActivity.Senha+"&cdProduto="+ codBarras); // Param URL para o WebView

                startActivity(intent);

                finish();

            }
        });
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {
        //Log.e(TAG, "onScannedMultiple: " + barcodes.size());

        //String codes = "";
        //for (Barcode barcode : barcodes) {
        //    codes += barcode.displayValue + ", ";
        //}

        //final String finalCodes = codes;
        //runOnUiThread(new Runnable() {
        //    @Override
        //    public void run() {
        //        Toast.makeText(getApplicationContext(), "Barcodes: " + finalCodes, Toast.LENGTH_SHORT).show();
        //    }
       // });
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {
        Toast.makeText(getApplicationContext(), "Camera permission denied!", Toast.LENGTH_LONG).show();
        finish();
    }
}